(function($) { 
    skel.breakpoints({ xlarge: '(max-width: 1680px)', large: '(max-width: 1280px)', medium: '(max-width: 980px)', small: '(max-width: 736px)', xsmall: '(max-width: 480px)', xxsmall: '(max-width: 360px)' });

    var $window = $(window),
    $body = $('body'),
    $wrapper = $('#wrapper'),
    $header = $('#header'),
    $blog_header = $('#blog_header'),
    $footer = $('#footer'),
    $main = $('#main'),
    $about_me = $('#about_me')
    $main_articles = $main.children('article');

    $body.addClass('is-loading');
    $(window).on('load', function() {
        //amplitude.getInstance().logEvent('site_load', {'type': 'site_load', 'page': location.href}); 
        window.setTimeout(function() { 
            $body.removeClass('is-loading'); 
        }, 300);
        window.setTimeout(function() { 
            $footer.removeClass('is-loading'); 
        }, 3000);
    });

    $('form').placeholder(); if (skel.vars.IEVersion < 12) { var flexboxFixTimeoutId;
        $window.on('resize.flexbox-fix', function() { clearTimeout(flexboxFixTimeoutId);
            flexboxFixTimeoutId = setTimeout(function() { if ($wrapper.prop('scrollHeight') > $window.height()) $wrapper.css('height', 'auto');
                else $wrapper.css('height', '100vh'); }, 250); }).triggerHandler('resize.flexbox-fix'); } var $nav = $header.children('nav'),
        $nav_li = $nav.find('li'); if ($nav_li.length % 2 == 0) { $nav.addClass('use-middle');
        $nav_li.eq(($nav_li.length / 2)).addClass('is-middle'); } 
        var delay = 325, locked = false;

    $main._show = function(id, initial) { var $article = $main_articles.filter('#' + id); if ($article.length == 0) return; if (locked || (typeof initial != 'undefined' && initial === true)) { $body.addClass('is-switching');
            $body.addClass('is-article-visible');
            $main_articles.removeClass('active');
            $header.hide();
            $footer.hide();
            $main.show();
            $article.show();
            $article.addClass('active');
            locked = false;
            setTimeout(function() { $body.removeClass('is-switching'); }, (initial ? 1000 : 0)); return; } locked = true; if ($body.hasClass('is-article-visible')) { var $currentArticle = $main_articles.filter('.active');
            $currentArticle.removeClass('active');
            setTimeout(function() { $currentArticle.hide();
                $article.show();
                setTimeout(function() { $article.addClass('active');
                    $window.scrollTop(0).triggerHandler('resize.flexbox-fix');
                    setTimeout(function() { locked = false; }, delay); }, 25); }, delay); } else { $body.addClass('is-article-visible');
            setTimeout(function() { $header.hide();
                $footer.hide();
                $main.show();
                $article.show();
                setTimeout(function() { $article.addClass('active');
                    $window.scrollTop(0).triggerHandler('resize.flexbox-fix');
                    setTimeout(function() { locked = false; }, delay); }, 25); }, delay); } };

    $main._hide = function(addState) { var $article = $main_articles.filter('.active'); if (!$body.hasClass('is-article-visible')) return; if (typeof addState != 'undefined' && addState === true) history.pushState(null, null, '#'); if (locked) { $body.addClass('is-switching');
            $article.removeClass('active');
            $article.hide();
            $main.hide();
            $footer.show();
            $header.show();
            $body.removeClass('is-article-visible');
            locked = false;
            $body.removeClass('is-switching');
            $window.scrollTop(0).triggerHandler('resize.flexbox-fix'); return; } locked = true;
        $article.removeClass('active');
        setTimeout(function() { $article.hide();
            $main.hide();
            $footer.show();
            $header.show();
            setTimeout(function() { $body.removeClass('is-article-visible');
                $window.scrollTop(0).triggerHandler('resize.flexbox-fix');
                setTimeout(function() { locked = false; }, delay); }, 25); }, delay); };

    $main_articles.each(function() { var $this = $(this);
        $('<div class="close">Close</div>').appendTo($this).on('click', function() { location.hash = ''; });
        $this.on('click', function(event) { event.stopPropagation(); }); });


    $body.on('click', function(event) { if ($body.hasClass('is-article-visible')) $main._hide(true); });

    //allow exit on esc
    $window.on('keyup', function(event) { 
        switch (event.keyCode) {
            case 27:
                if ($body.hasClass('is-article-visible')) $main._hide(true); break;
            default:
                break; 
        } 
    });

    $header.on('click', '.namebox', function(event) {
        if ($about_me.hasClass('is-hidden')) {
            $about_me.removeClass('is-hidden');
        }
        else {
            $about_me.addClass('is-hidden');
        }
    }); 

    $('#kitten-it').on('click', function(event) {
        $('#bg').css('background-image', "url({{url_for('static', filename='images/d2-lookalike.jpeg') }})"); 
        $('#bg').toggleClass('special');
    });

    $('#flip-it').on('click', function(event) {
        if ($('#traits').hasClass('is-flipped')) {
            $('#traits').removeClass('is-flipped');
        }
        else {
            $('#traits').addClass('is-flipped');
        }
    }); 

    $window.on('hashchange', function(event) { 
        if (location.hash == '' || location.hash == '#') { 
            event.preventDefault();
            event.stopPropagation();
            $main._hide(); 
        } 
        else if ($main_articles.filter(location.hash).length > 0) { 
            event.preventDefault();
            event.stopPropagation();
            $main._show(location.hash.substr(1)); 
        }
        //amplitude.getInstance().logEvent('page_change', {'type': 'page_change', 'value': location.hash, 'page':location.href});            
    });
    $body.click(function(event) {
        let text = $(event.target).text() ? $(event.target).text() : $(event.target).attr("alt")
        if (text.length > 13) {
            text = $(event.target).closest('[id]').attr("id")
        }
        //amplitude.getInstance().logEvent('click', {'type': 'click', 'value': text, 'page': location.href}); 
    });

    if ('scrollRestoration' in history) {
        history.scrollRestoration = 'manual';
    }
    else { 
        var oldScrollPos = 0,
            scrollPos = 0,
            $htmlbody = $('html,body');
        $window.on('scroll', function() { oldScrollPos = scrollPos;
            scrollPos = $htmlbody.scrollTop(); }).on('hashchange', function() { $window.scrollTop(oldScrollPos); }); 
    } 

    $main.hide();
    $main_articles.hide(); 
    if (location.hash != '' && location.hash != '#') {
        $window.on('load', function() { 
            $main._show(location.hash.substr(1), true);
        });
    }
})(jQuery);