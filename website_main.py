# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, redirect
from flask_frozen import Freezer

app = Flask(__name__)
app.config['FREEZER_BASE_URL'] = 'https://tylerbs.com'
app.config['FREEZER_DESTINATION'] = 'public'
FREEZER_RELATIVE_URLS = True
freezer = Freezer(app)

@app.cli.command()
def freeze():
    freezer.freeze()


@app.cli.command()
def serve():
    freezer.run()


class bubble_icon:
    def __init__(self, icon, label, id=None, href=None):
        self.icon=icon
        self.label=label
        self.id=id
        self.href=href

    def tag_a(self):
        return ' '.join([
            f'class=\"{self.icon}\"',
            f'id=\"{self.id}\"' if self.id else '',
            f'href=\"{self.href}\"' if self.href else '',
            ])

class table_link:
    def __init__(self, icon, label, href):
        self.icon=icon
        self.label=label
        self.href=href

bubble_icons = {
    'gitlab': bubble_icon(
        'icon fa-gitlab',
        'Gitlab',
        '',
        href='https://gitlab.com/tybodsm'
        ),
    'linkedin': bubble_icon(
        'icon fa-linkedin-square',
        'LinkedIn',
        '',
        href='https://www.linkedin.com/in/tyler-bodine-smith-61669b59'
        ),
    # 'resume': bubble_icon(
    #     'icon fa-clipboard',
    #     'Resume',
    #     '',
    #     ),
    'spotify': bubble_icon(
        'icon fa-spotify',
        'Spotify',
        '',
        href='https://open.spotify.com/user/tybodsm'
        ),
    'cat': bubble_icon(
        'icon fa-paw',
        'D2',
        id='kitten-it',
        ),
    'australia': bubble_icon(
        'icon fa-arrows-v',
        'Australian Mode',
        id='flip-it'
        ),
}

intl_links = {
    'academic': [
        table_link(
            'fa-university',
            'Peter Shott Data (Yale)',
            'http://faculty.som.yale.edu/peterschott/sub_international.htm'
            ),
        
        table_link(
            'fa-university',
            'Rob Feenstra Data (UC Davis)',
             'http://cid.econ.ucdavis.edu/'
             ),
        ],

    'general': [
        table_link(
            'fa-globe',
            'World Integrated Trade Solutions',
            'http://wits.worldbank.org/'
            ),
        
        table_link(
            'fa-globe',
            'BEA international data',
            'https://www.bea.gov/international/'
            ),
        ],

    'trade': [        
        table_link(
            'fa-ship',
            'Census Foreign Trade',
            'https://www.census.gov/foreign-trade/index.html'
            ),
        
        table_link(
            'fa-ship',
            'Census Related-Party Trade',
            'https://www.census.gov/programs-surveys/trade/data/tables/relatedparty.html'
            ),

        table_link(
            'fa-ship',
            'UN Comtrade',
             'https://comtrade.un.org/'
             ),
    
        table_link(
            'fa-ship',
            'IMF Direction of Trade Statistics',
             'http://data.imf.org/dot'
             ),
        ],

    'financial': [  
        table_link(
            'fa-money',
            'IMF Balance of Payments',
            'http://data.imf.org/BOP',
            ),
        ],
}

pub_links = [
    ['“Why Renegotiating NAFTA Could Disrupt Supply Chains”'
        ,'http://libertystreeteconomics.newyorkfed.org/2017/04/why-renegotiating-nafta-could-disrupt-supply-chains.html'],
    ['“Whats Driving the Recent Slump in U.S. Imports?”'
        ,'http://libertystreeteconomics.newyorkfed.org/2016/11/whats-driving-the-recent-slump-in-us-imports.html'],
    ['“The Effect of the Strong Dollar on U.S. Growth”'
        ,'http://libertystreeteconomics.newyorkfed.org/2015/07/the-effect-of-the-strong-dollar-on-us-growth.html'],
    ['“Did the West Coast Port Dispute Contribute to the First-Quarter GDP”'
        ,'http://libertystreeteconomics.newyorkfed.org/2015/07/did-the-west-coast-port-dispute-contribute-to-the-first-quarter-gdp-slowdown.html'],
]

name = 'Tyler Bodine-Smith'
job = 'Data Scientist'
email = 'tbs@tylerbs.com'
img_background = 'road-2069519.jpg'

banners = {
    'code': '/static/images/code_resized.jpg',
    'work': '/static/images/work_resized.jpg',
    'research': '/static/images/research1_resized.jpg',
    'personal': '/static/images/personal_resized.jpg',
}
intl_banner = '/static/images/banner_trade.jpg'


def page(file, kwargs):
    kwargs['bubble_icons'] = bubble_icons
    kwargs['title'] = "Basecamp"
    kwargs['name'] = name
    kwargs['job'] = job
    kwargs['email'] = email
    return render_template(
        file,
        **kwargs
        )

@app.route('/')
def home():
    kwargs = {}
    kwargs['intl_links'] = intl_links
    kwargs['pub_links'] = pub_links
    kwargs['banners'] = banners

    return page('home.html', kwargs)


if __name__ == "__main__":
    app.run(debug=False )

